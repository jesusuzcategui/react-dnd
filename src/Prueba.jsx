import { useState, useRef } from 'react';
import { DragDropContext, Draggable, Droppable } from 'react-beautiful-dnd';
import './CardTask.css';

function Prueba() {
  const lista = [
    { idItem: "1", id: 1, name: 'Pepe 1' },
    { idItem: "2", id: 2, name: 'Pepe 2' },
    { idItem: "3", id: 3, name: 'Pepe 3' },
    { idItem: "4", id: 4, name: 'Pepe 4' },
  ];
  
  const lista2 = [
    { idItem: "A", id: 1, name: 'Box 1' },
    { idItem: "B", id: 2, name: 'Box 2' },
    { idItem: "C", id: 3, name: 'Box 3' },
    { idItem: "D", id: 4, name: 'Box 4' },
  ];
  
  const [characters, updateCharacters] = useState(lista);
  const [characters2, updateCharacters2] = useState(lista2);

  const handleOnDragEnd = (result) => {
    const items = Array.from(characters);
    const [reorderedItem] = items.splice(result.source.index, 1);
    items.splice(result.destination.index, 0, reorderedItem);
    updateCharacters(items);
  };

  return (
    <div className="App">
      <DragDropContext className="flexbox" onDragEnd={handleOnDragEnd}>
        <div className="cajita1">
          <Droppable droppableId="characters">
            {(provided) => (
              <ul className="characters" {...provided.droppableProps} ref={provided.innerRef}>
                {characters.map((item, i) => {
                  return (
                    <div key={i}>
                      <Draggable key={item.idItem} draggableId={item.idItem} index={i}>
                        {(provided) => (
                          <div className="cardTask" ref={provided.innerRef} {...provided.draggableProps} {...provided.dragHandleProps}>{item.name}</div>
                        )}
                      </Draggable>
                      {provided.placeholder}
                    </div>
                  );
                })}
              </ul>
            )}
          </Droppable>
        </div>
        <div className="cajita2">
          <Droppable droppableId="characters2">
            {(provided) => (
              <ul className="characters2" {...provided.droppableProps} ref={provided.innerRef}>
                {characters2.map((item, i) => {
                  return (
                    <div key={i}>
                      <Draggable key={item.idItem} draggableId={item.idItem} index={i}>
                        {(provided) => (
                          <div className="cardTask" ref={provided.innerRef} {...provided.draggableProps} {...provided.dragHandleProps}>{item.name}</div>
                        )}
                      </Draggable>
                      {provided.placeholder}
                    </div>
                  );
                })}
              </ul>
            )}
          </Droppable>
        </div>
      </DragDropContext>
    </div>
  )
}

export default Prueba;