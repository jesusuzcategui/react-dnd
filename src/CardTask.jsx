import React, { useRef } from 'react';
import style from './CardTask.css';

const CardTask = () => {
    const ref = useRef(null);
    return (
        <>
            <div className={style.cardTask} ref={ref}>
                Tarea
            </div>
        </>
    );
};

export default CardTask;